<?php return array (
  'barryvdh/laravel-ide-helper' => 
  array (
    'providers' => 
    array (
      0 => 'Barryvdh\\LaravelIdeHelper\\IdeHelperServiceProvider',
    ),
  ),
  'beyondcode/laravel-dump-server' => 
  array (
    'providers' => 
    array (
      0 => 'BeyondCode\\DumpServer\\DumpServerServiceProvider',
    ),
  ),
  'botman/botman' => 
  array (
    'providers' => 
    array (
      0 => 'BotMan\\BotMan\\BotManServiceProvider',
    ),
    'aliases' => 
    array (
      'BotMan' => 'BotMan\\BotMan\\Facades\\BotMan',
    ),
  ),
  'botman/driver-telegram' => 
  array (
    'providers' => 
    array (
      0 => 'BotMan\\Drivers\\Telegram\\Providers\\TelegramServiceProvider',
    ),
  ),
  'botman/driver-web' => 
  array (
    'providers' => 
    array (
      0 => 'BotMan\\Drivers\\Web\\Providers\\WebServiceProvider',
    ),
  ),
  'botman/studio-addons' => 
  array (
    'providers' => 
    array (
      0 => 'BotMan\\Studio\\Providers\\StudioServiceProvider',
      1 => 'BotMan\\Studio\\Providers\\RouteServiceProvider',
    ),
  ),
  'botman/tinker' => 
  array (
    'providers' => 
    array (
      0 => 'BotMan\\Tinker\\TinkerServiceProvider',
    ),
  ),
  'fideloper/proxy' => 
  array (
    'providers' => 
    array (
      0 => 'Fideloper\\Proxy\\TrustedProxyServiceProvider',
    ),
  ),
  'irazasyed/telegram-bot-sdk' => 
  array (
    'providers' => 
    array (
      0 => 'Telegram\\Bot\\Laravel\\TelegramServiceProvider',
    ),
    'aliases' => 
    array (
      'Telegram' => 'Telegram\\Bot\\Laravel\\Facades\\Telegram',
    ),
  ),
  'laravel/tinker' => 
  array (
    'providers' => 
    array (
      0 => 'Laravel\\Tinker\\TinkerServiceProvider',
    ),
  ),
  'nesbot/carbon' => 
  array (
    'providers' => 
    array (
      0 => 'Carbon\\Laravel\\ServiceProvider',
    ),
  ),
  'nunomaduro/collision' => 
  array (
    'providers' => 
    array (
      0 => 'NunoMaduro\\Collision\\Adapters\\Laravel\\CollisionServiceProvider',
    ),
  ),
);